# Nginx Access Log Parser

Bash snippets for extracting IP, Request and determine Country from nginx access.log

##### Task #1
> Parse the file and generate report: a sorted list of IP address and number of requests per each of them. E.g.
  1.1.1.1 12
  43.12.65.11 8
  Do it in at least 2 different ways

###### Solution
```sh
cat access.log | cut -d ' ' -f 1 | sort | uniq -c | sort -nr | head
awk '{print $1}' access.log | sort | uniq -c | sort -nr | head
```
##### Task #2
> Add 3rd column which shows the country where this IP address belongs to. E.g.
  1.1.1.1 12 UA

###### Solution
```sh
cat access.log |awk '{print $1}' | sort | uniq -c | sort -nr | head | while read line; do
        ip=$(echo $line | awk '{print $2}')
        count=$(echo $line | awk '{print $1}')
        country=$(curl -s https://ipapi.co/${ip}/country)
        echo ${count} ${ip} ${country}
done
```